﻿# Japanese voice from jTOS to iTOS (Updated Mar 12, aka jTOS first EP12 patch) #

------------------------------

## Installation ##

### Right-click [Here](https://bitbucket.org/idzkbpx/itos-stuffs/raw/master/JP%20Voice/iTOS-JPVoice-ep12-jtos-based-%F0%9F%96%95%F0%9F%90%A5.ipf) and click ***save link as*** and put it to `/patch/` folder instead of `/data/` ~~for the meme~~ to be working as intended ###

ps. please make sure that the file has `🖕🐥` in the file name or IMC's patcher will delete it

And lastly, go to `<base folder of tos>/release/user.xml` and make sure that `Language` param inside `<Sound` tag *(the second to last line)* is `Japanese`, if not, change it, save, and enjoy

------------------------------

## What??? ##

IMC has pushed '4ever' sound configuration into iTOS since the end of [2019's patch](https://steamdb.info/patchnotes/4534213/).
[Then they updated `skilvoice_jap.fsb` with Korean voice as placeholders](https://kbpx.is-inside.me/gxdkr18X.png)

Later, on March 11, [jTOS got EP12 update](https://treeofsavior.jp/page/newsview.php?n=130). and their soundbanks is different from iTOS. since they're replacing placeholder voices to proper Japanese one

example: [https://youtu.be/gLgIq1QYtSQ](https://youtu.be/gLgIq1QYtSQ)

*Source? I datamined it. xD*

------------------------------

## Thanks ##

- [tpIpfTool](https://github.com/kuronekotei/IpfTool), for `*.ipf` tool that can drag&drop, much easier to use
- some bl-- *This post was flagged by the community and is temporarily hidden.*
- Oh, yes, IMC with what a nice way to bloat the client size with patches, ~6GB for based game, more than 15GB for patches ♥
